
(function ($, Drupal) {
  Drupal.behaviors.LEGISLayouts = {
    attach: function (context, settings) {
      
      $('.panels-legis-tabs .panels-tabs .panels-tab a').click(function(event) {
        event.preventDefault();
        var TabsID = $(this).attr('data-tabs');
        var contentClass = $(this).attr('href').replace('#', '');
        
        // Add active/inactive class to the content:
        $('#' + TabsID + ' .panels-tab-content.active').removeClass('active').addClass('inactive');
        $('#' + TabsID + ' .panels-tab-content.' + contentClass).removeClass('inactive').addClass('active');

        // Add active/inactive class to the label:
        $(this).parent().parent().find('.panels-tab.active').removeClass('active').addClass('inactive');
        $(this).parent().removeClass('inactive').addClass('active');
        
      });
      
    }
  };
})(jQuery, Drupal);