const tabLink = jQuery(".tab-link");

//Evento clic para mostrar contenido
tabLink.click(function (e) {
  e.preventDefault();
  tabs(jQuery(this));
});

//Función encargada de mostrar el contenido según clic
function tabs(ele) {
  //Quitar active de los demás li
  jQuery('#tabsCustom #tabs li').removeClass("active");
  //Agregar activo al elemento cliqueado
  jQuery(ele).parent().addClass("active");
  //Toma los hijos de tabContent
  const tabContent = jQuery("#tabContent").children();
  //Mostrar el contenido correspondiente al cliqueado
  jQuery.each(tabContent, function (ix, vl) {
    if (ele.attr("data-tab") === jQuery(vl).attr("data-tab")) {
      jQuery(vl).show();
    } else {
      jQuery(vl).hide();
    }
  });
}

