<?php

namespace Drupal\baluarte_layouts\Plugin\Layout;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Plugin\PluginFormInterface;

class Layout3Collaps extends LayoutDefault implements PluginFormInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'tab0_title' => 'Nombre 1',
      'tab1_title' => 'Nombre 2',
      'tab2_title' => 'Nombre 3',
      'tabs_id' => 'accordion-' . time(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();
    $form['tabs_id'] = [
      '#type' => 'textfield',
      '#title' => 'ID del accordion',
      '#default_value' => $configuration['tabs_id'],
      '#required' => TRUE,
    ];
    $form['tab0_title'] = [
      '#type' => 'textfield',
      '#title' => 'Nombre del collpase uno',
      '#default_value' => $configuration['tab0_title'],
    ];
    $form['tab1_title'] = [
      '#type' => 'textfield',
      '#title' => 'Nombre del collpase dos',
      '#default_value' => $configuration['tab1_title'],
    ];
    $form['tab2_title'] = [
      '#type' => 'textfield',
      '#title' => 'Nombre del collpase tres',
      '#default_value' => $configuration['tab2_title'],
    ];
    return $form;
  }

  /**
   * @inheritDoc
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $this->configuration['tabs_id'] = $form_state->getValue('tabs_id');
    $this->configuration['tab0_title'] = $form_state->getValue('tab0_title');
    $this->configuration['tab1_title'] = $form_state->getValue('tab1_title');
    $this->configuration['tab2_title'] = $form_state->getValue('tab2_title');
  }

}
