<?php

namespace Drupal\baluarte_simple_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\text\Plugin\Field\FieldWidget\TextareaWidget;

/**
 * Plugin implementation of the 'legis_text_area_basic_widget' widget.
 *
 * @FieldWidget(
 *   id = "Text_area_basic_widget",
 *   label = @Translation("Text area basic widget"),
 *   field_types = {
 *     "text_with_summary"
 *   }
 * )
 */
class TextAreaBasicWidget extends TextareaWidget {
  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'rows' => '9',
      'summary_rows' => '3',
      'placeholder' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['value'] = $element + [
      '#type' => 'textarea',
      '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : NULL,
      '#placeholder' => $this->getSetting('placeholder'),
    ];

    return $element;
  }
}
