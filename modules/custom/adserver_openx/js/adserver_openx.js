/**
 * @file
 * Contains the definition of the behaviour jsTestBlackWeight.
 */

(function ($, Drupal, drupalSettings) {
  "use strict";

  /**
   * Attaches the JS test behavior to to weight div.
   */
  Drupal.behaviors.jsrevive = {
    attach: function (context, settings) {
      let server = drupalSettings.server;
      let s = document.createElement("script");
      s.type = "text/javascript";
      s.async = true;
      s.src = "//" + server + "/asyncjs.php";
      
      let x = document.getElementsByTagName("script")[1],
        xVal = document.getElementsByTagName("script")[1].src;

      if (!xVal.includes(s.src)) {
        x.parentNode.insertBefore(s, x);
      }

    },
  };
})(jQuery, Drupal, drupalSettings);
