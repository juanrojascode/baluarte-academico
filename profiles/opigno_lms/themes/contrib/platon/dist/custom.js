(function ($, Drupal, drupalSettings) {
  $(document).ready(function () {
    menuCollapse();
    removeAdvertisingFeed();
    editPlaceholderForm();
    viewImagenOfEvent();

    $('#carouselEvents').carousel({
      interval: 4000,
      pause: false
    })

    $('.user .user-btn').click(function (e) { 
      e.preventDefault();
      $('.user .dropdown-menu').toggleClass('show');
    });
  });

  function menuCollapse() {
    const mnIcon = $(".header-bottom__menu-icon");
    const mnEle = $(".header-bottom__menu-ele");
    $(mnIcon).click(function (e) {
      e.preventDefault();
      mnEle.toggleClass("show animated");
      mnEle.toggleClass("");
    });
  }

  function removeAdvertisingFeed() {
    let n = 0;
    let eventInterval = () => {
      if ($(".eapps-link").length) {
        $(".eapps-link").remove();
        n = 1;
      }
      n = 1 ? clearInterval(interval) : null;
    };
    let interval = setInterval(eventInterval, 1000);
  }

  function editPlaceholderForm() {
    const form = $('#contact-message-feedback-form');
    let count = 0;
    $.each( form.find('.form-item') , function (id, ele) { 
      let val = $(ele).find('label').text();
      //console.log(val);
      $(ele).find('input').attr('placeholder', val);
      if (id == 4) {
        $(ele).find('textarea').attr('placeholder', val);
      }
    });
    
  }

  function viewImagenOfEvent() {
    $('.viewImage').click(function (e) { 
      e.preventDefault();
      let src = $(this).siblings().text();
      console.log('clic');
      console.log( src );
      $('#modalImgEvent .modalImage').attr('src', src);
      $('#modalImgEvent').modal('show');
    });
  }

  /*
   * Traducciones
   */
  $("#search-form input").attr("placeholder", "Buscar...");
  $(".loggin input").attr("placeholder") == "username" ? true : false;
})(window.jQuery, window.Drupal, window.drupalSettings);
